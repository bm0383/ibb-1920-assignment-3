import cv2
import numpy as np
from PIL import Image
import time
from random import randint

def imshow_components(labels):
    label_hue = np.uint8(179*labels/np.max(labels))
    blank_ch = 255*np.ones_like(label_hue)
    labeled_img = cv2.merge([label_hue, blank_ch, blank_ch])
    labeled_img = cv2.cvtColor(labeled_img, cv2.COLOR_HSV2BGR)

    labeled_img[label_hue==0] = 0

    cv2.imshow('labeled.png', labeled_img)
    cv2.waitKey(100)

sequences = ['PETS09-S2L1']
display = True
phase = 'train'
total_time = 0.0
total_frames = 0
fgbg = cv2.createBackgroundSubtractorMOG2()

for seq in sequences:
    print("Processing %s." % (seq))
    for frame in range(0,200):
        frame += 1  # detection and frame numbers begin at 1
        print(frame)
        fn = '2DMOT2015/%s/%s/img1/%06d.jpg' % (phase, seq, frame)
        print(fn)
        img = cv2.imread(fn, 1)
        difference = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        difference = fgbg.apply(difference)
        cv2.imshow("differce", difference)

        _,difference = cv2.threshold(difference,30,255,cv2.THRESH_BINARY)

        kernel = np.ones((2, 1), np.uint8)
        opening = cv2.morphologyEx(difference, cv2.MORPH_ERODE, kernel)
        cv2.imshow("erosion1", opening)

        kernel = np.ones((6,3), np.uint8)
        opening = cv2.morphologyEx(opening, cv2.MORPH_OPEN, kernel)
        cv2.imshow("erosion2", opening)

        kernel = np.ones((2,1), np.uint8)
        opening = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)
        cv2.imshow("erosion3", opening)

        ret, labels, stats, centroids = cv2.connectedComponentsWithStats(opening)

        min_size = 140

        sizes = stats[1:, -1];
        ret = ret - 1
        img2 = np.zeros((labels.shape),dtype=np.uint8)
        # for every component in the image, you keep it only if it's above min_size
        for i in range(0, ret):
            if sizes[i] >= min_size:
                img2[labels == i + 1] = 255

        #cv2.imshow("First frame", img2)

        _, difference = cv2.threshold(img2, 1, 255, cv2.THRESH_BINARY)
        cv2.imshow("testni", difference)
        ret, labels, stats, centroids = cv2.connectedComponentsWithStats(difference)
        for i in range(1,len(stats)):

            #should check area also
            curr_state = stats[i]
            if curr_state[4] > 20:
                x1 = curr_state[0]
                y1 = curr_state[1]
                x2 = x1+curr_state[2]
                y2 = y1+curr_state[3]
                cv2.rectangle(img, (x1, y1), (x2, y2), (255,0,0), 2)
                font = cv2.FONT_HERSHEY_SIMPLEX
                cv2.putText(img, str(i), (int(centroids[i][0]),int(centroids[i][1])), font, 0.5, (0, 255, 0), 2, cv2.LINE_AA)
            cv2.imshow('Detection', img)

        imshow_components(labels)

        #cv2.imshow("Frame", frame)

        #cv2.imshow("difference", difference)

        # show frame
        #cv2.imshow('MultiTracker', frame)

        key = cv2.waitKey(3000)
        if key == 27:
            break

cv2.destroyAllWindows()
