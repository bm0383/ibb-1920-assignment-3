from __future__ import print_function
import os.path
import numpy as np
from skimage.measure import compare_ssim
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from skimage import io
from sklearn.utils.linear_assignment_ import linear_assignment
import glob
import time
import argparse
from filterpy.kalman import KalmanFilter
import cv2
from skimage import feature
from skimage.measure import compare_ssim as ssim
import time


_img = [] # stores clean image
_img_dirty = [] # stores image to draw rectangle during cropping
_points = [] # stores the points for the cropping


class LocalBinaryPatterns:
    def __init__(self, numPoints, radius):
        # store the number of points and radius
        self.numPoints = numPoints
        self.radius = radius

    def describe(self, image, eps=1e-7):
        # compute the Local Binary Pattern representation
        # of the image, and then use the LBP representation
        # to build the histogram of patterns
        lbp = feature.local_binary_pattern(image, self.numPoints,
                                           self.radius, method="uniform")
        (hist, _) = np.histogram(lbp.ravel(),
                                 bins=np.arange(0, self.numPoints + 3),
                                 range=(0, self.numPoints + 2))

        # normalize the histogram
        hist = hist.astype("float")
        hist /= (hist.sum() + eps)

        # return the histogram of Local Binary Patterns
        return hist

def calc_histogram(patch):

    # samo povecevanje stevila binov ne vpliva bistveno na boljse delovanje trackerja,
    # ima pa velik vpliv na hitrost delovanja.

    #TODO POSKUS KOMBINIRANJA HISTOGRAMA Z SSIM MERO
    #Ne deluje najbolje...  ssim deluje boljse

    #TODO MOGOCE GLEJ HISTOGRAME, KO PA PRIDE DO PREKIRAVANJA DVEH TRACKERJEV PA GLEJ SSIM?

    num_bins = 16

    mask = None

    blue_model = cv2.calcHist([patch], [0], mask, [num_bins],  [0,256]).flatten()
    green_model = cv2.calcHist([patch], [1], mask, [num_bins],  [0,256]).flatten()
    red_model = cv2.calcHist([patch], [2], mask, [num_bins],  [0,256]).flatten()

    color_patch = np.concatenate((blue_model, green_model, red_model))

    # Normalize histogram values for the KL divergence computation
    color_patch = color_patch/np.sum(color_patch)

    return color_patch


class ParticleFilter:

    def __init__(self, s_t, color_model,w_t,likelihood,object_bound,id,num_of_low):
        self.s_t = s_t
        self.color_model = color_model
        self.w_t = w_t
        self.likelihood = likelihood
        self.object_bound = object_bound
        self.id = id
        self.num_of_low = num_of_low

    def return_id(self):
        return self.id

    def return_oject_bound(self):
        return self.object_bound

    def extract_patch(self, image,temp_st):
      """ Extracts the part of the image corresponding to a given state. """
      temp_st = temp_st.astype(int) # Convert state to discrete pixel locations
      return image[temp_st[0]:temp_st[0]+self.object_bound[0],temp_st[1]:temp_st[1]+self.object_bound[1]]

    def appearance_model(self, image,temp_st):
      # Hyperparameters
      l = 0.8 # lambda
      patch = self.extract_patch(image,temp_st)

      #color_patch = calc_histogram(patch)

      #TODO poskus vec razlicnih mer razdale med histogrami
      #Najbolje se obnese cv2.HISTCMP_BHATTACHARYYA, ostali so slabsi + potrebno je menjati
      #mero kater je bolj podoben in kater manj


      #divergence = cv2.compareHist(self.color_model, color_patch, cv2.HISTCMP_BHATTACHARYYA)
      #likelihood = 1 - divergence

      likelihood = ssim(patch, self.color_model, multichannel=True, data_range=255)

      return likelihood

    def motion_model(self, image_bound):

      std_dev = 17

      # Motion estimation
      self.s_t = self.s_t + std_dev*np.random.randn(self.s_t.shape[0],2)

      # Out-of-bounds check
      self.s_t[:,0] = np.maximum(0,np.minimum(image_bound[0]-self.object_bound[0], self.s_t[:,0]))
      self.s_t[:,1] = np.maximum(0,np.minimum(image_bound[1]-self.object_bound[1], self.s_t[:,1]))

    def random_sample(self):

      cumsum = np.cumsum(self.w_t)
      draws = np.random.rand(self.w_t.shape[0])
      idxs = np.zeros(self.w_t.shape[0])
      for i,draw in enumerate(draws):
          for j,probability in enumerate(cumsum):
              if probability > draw:
                  idxs[i] = j
                  break
      return idxs.astype(int)


    def move(self,img):
        idxs = self.random_sample()
        self.s_t = self.s_t[idxs,:]
        self.w_t = self.w_t[idxs]

        # Move particles according to motion model
        self.motion_model(img.shape)

        # Compute appearance likelihood for each particle
        for j in range(NUM_PARTICLES):
            sf = self.s_t[j,:]
            temp = self.appearance_model(img, sf)

            self.likelihood[j] = temp



        # Update particle weights
        self.w_t = self.w_t*self.likelihood
        self.w_t = self.w_t/np.sum(self.w_t)

        # Estimate object location based on weighted
        # states of the particles.
        estimate_t = (self.s_t.T.dot(self.w_t)).astype(int)
        return estimate_t

    def draw(self, img, estimate,NUM_PARTICLES):
        """ DRAWING """

        # Draw box around the mean estimate
        cv2.rectangle(img,(estimate[1],estimate[0]),
                      (estimate[1]+self.object_bound[1],estimate[0]+self.object_bound[0]),
                      (0,255,0),2)

        cv2.circle(img, (estimate[1],estimate[0]),
                   10, (0, 0, 255))

        cv2.putText(img, str(self.get_confidience()) , (estimate[1],estimate[0]),
                    cv2.QT_FONT_NORMAL, 0.5, (255, 0, 0), 2, cv2.LINE_AA)

        cv2.imshow("test", img)

    def set_num_of_low(self, number):
        self.num_of_low = number


    def get_num_of_low(self):
        return self.num_of_low

    def get_confidience(self):
        return np.max(self.likelihood)

    def update_model(self,image):
        #get the best matching particle
        alfa = 0.00
        best = -1
        index = -1
        for j in range(NUM_PARTICLES):
            sf = self.s_t[j,:]
            temp = self.appearance_model(img, sf)
            if temp > best:
                best = temp
                index = j

        patch = self.extract_patch(image, self.s_t[index,:])
        color_patch = calc_histogram(patch)
        self.color_model = (1-alfa)*self.color_model + alfa * color_patch

def area(a, b):
    dx = min(a[2], b[3]) - max(a[0], b[1])
    dy = min(a[3], b[2]) - max(a[1], b[0])
    if (dx >= 0) and (dy >= 0):
        return dx * dy

if __name__ == '__main__':
  #sequences = ['ADL-Rundle-6','ADL-Rundle-8','ETH-Bahnhof','ETH-Pedcross2','ETH-Sunnyday','KITTI-13','KITTI-17','PETS09-S2L1','TUD-Campus','TUD-Stadtmitte','Venice-2']
  sequences = ['PETS09-S2L1']
  display = True
  phase = 'train'
  total_time = 0.0
  total_frames = 0
  colours = np.random.rand(32,3) #used only for display

  if not os.path.exists('output'):
    os.makedirs('output')

  NUM_PARTICLES = 100
  np.random.seed(0)
  start = time.time()
  for seq in sequences:
    seq_dets = np.loadtxt('data/%s/det.txt'%(seq),delimiter=',') #load detections
    with open('output/%s.txt'%(seq),'w') as out_file:
      print("Processing %s."%(seq))
      trackers = []
      for frame in range(int(seq_dets[:,0].max())):
            frame += 1  # detection and frame numbers begin at 1
            dets = seq_dets[seq_dets[:, 0] == frame, 2:7]
            dets[:, 2:4] += dets[:, 0:2]  # convert to [x1,y1,w,h] to [x1,y1,x2,y2]
            total_frames += 1
            fn = '2DMOT2015/%s/%s/img1/%06d.jpg' %(phase, seq, frame)
            img = cv2.imread(fn, 1)
            #img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

            masking = np.full((img.shape[0], img.shape[1]), 0, dtype=np.uint8)
            for i in range(0,len(dets)):
                #TODO MASK OUT ALL NON INTERESTING STUFF
                single_det = dets[i]
                masking[int(single_det[1]):int(single_det[3]), int(single_det[0]):int(single_det[2])] = -1
            print(frame)
            res = cv2.bitwise_and(img,img,mask = masking)
            temp = img
            img = res

            if(frame == 1):
                for i in range(0,len(dets)):
                    single_det = dets[i]
                    interesting = img[int(single_det[1]):int(single_det[3]), int(single_det[0]):int(single_det[2])]

                    #specificno za particle tracker
                    object_bound = interesting.shape
                    image_bound = img.shape
                    color_model = calc_histogram(interesting)
                    s_t = np.random.rand(NUM_PARTICLES, 2)

                    #TODO: kako bi prisil particle da so na zacetku okoli object bounda?
                    s_t[:, 0] = single_det[1]
                    s_t[:, 1] = single_det[0]

                    w_t = np.ones(NUM_PARTICLES) / NUM_PARTICLES
                    likelihood = np.zeros(NUM_PARTICLES)
                    tracked = ParticleFilter(s_t, interesting, w_t, likelihood, object_bound,i,0)
                    trackers.append(tracked)

            estimations = []
            temp_trackers = []
            for i in range(0,len(trackers)):
                estimate = trackers[i].move(img)

                # UPDATE MODELA samo poslabsa rezulatate ( tudi pri zelo majhni alfi)

                #UPDATE MODELA
                #trackers[i].update_model(img)

                trackers[i].draw(temp, estimate, NUM_PARTICLES)
                id = trackers[i].return_id()
                #TODO mogoce gledat za zadnjih 5 confidiencov?
                #TODO pogledat ali se trenutn tracker prekriva s katerim interesting pointom in sele nato prever
                #TODO jati se confidience

                # TODO mogoce tako da bi gledal kater interesting point nima preseka z katerim od trackerjev
                count = 0
                for t in range(0, len(dets)):
                    single_det = dets[t]
                    new_object_bound = trackers[i].return_oject_bound()
                    estimated_temp = [estimate[0], estimate[1], new_object_bound[0] + estimate[0],
                                            new_object_bound[1] + estimate[1]]
                    if area(single_det,estimated_temp) is None:
                        count = count + 1

                #TODO ce  se dva trackerja prekrivata ne deletaj ce confidience pade pod prag
                #ALSO particle number bi lahko bil lasten vsakemu trackerju... lahko povecas ce pride pod prag
                #done, ni nic bolje... brisi


                if(count == len(dets) or trackers[i].get_confidience() < 0.1):
                    #delete tracker

                    #poglej ce tracker pokriva dva interesting pointa, ce se tukaj ne glej confidience scora
                    track_count = 0
                    for z in range(0, len(dets)):
                        single_det = dets[z]

                        new_object_bound = trackers[i].return_oject_bound()
                        estimated_temp = [estimate[0], estimate[1], new_object_bound[0] + estimate[0],
                                          new_object_bound[1] + estimate[1]]

                        ar = area(single_det, estimated_temp)
                        if ar is not None:
                            track_count = track_count + 1
                    if track_count < 2:
                        trackers[i].set_num_of_low((trackers[i].get_num_of_low()) + 1)


                    if trackers[i].get_num_of_low() > 1 or count == len(dets):
                        print("delete")
                    else:
                        temp_trackers.append(trackers[i])
                        new_object_bound = trackers[i].return_oject_bound()
                        estimations.append([estimate[0], estimate[1], new_object_bound[0] + estimate[0],
                                            new_object_bound[1] + estimate[1]])

                        print('%d,%d,%.2f,%.2f,%.2f,%.2f,1,-1,-1,-1' % (
                        frame, id, estimate[1], estimate[0], new_object_bound[1], new_object_bound[0]),
                              file=out_file)
                else:
                    trackers[i].set_num_of_low(0)
                    temp_trackers.append(trackers[i])
                    new_object_bound = trackers[i].return_oject_bound()
                    estimations.append([estimate[0], estimate[1], new_object_bound[0] + estimate[0], new_object_bound[1]+ estimate[1]])

                    print('%d,%d,%.2f,%.2f,%.2f,%.2f,1,-1,-1,-1' % (frame, id, estimate[1],estimate[0],new_object_bound[1],new_object_bound[0]),
                          file=out_file)

            # TODO Pogledat kako bi lahko dodajal nove trackerje?
            # TODO mogoce tako da bi gledal kater interesting point nima preseka z katerim od trackerjev
            for i in range(0, len(dets)):
                single_det = dets[i]
                count = 0
                for j in range(0, len(estimations)):
                    ar = area(single_det, estimations[j])
                    if ar is None:
                        count = count + 1
                        #should be none len(trackerjev)
                #TODO mogoce za dalj casa?
                if count == len(estimations):
                    #noben od trackerjev ne pokriva tega podrocja
                    print("nov")
                    single_det = dets[i]
                    interesting = img[int(single_det[1]):int(single_det[3]), int(single_det[0]):int(single_det[2])]
                    #cv2.imshow("test",interesting)
                    #key = cv2.waitKey(10000) & 0xFF
                    # specificno za particle tracker
                    object_bound = interesting.shape
                    image_bound = img.shape
                    color_model = calc_histogram(interesting)
                    s_t = np.random.rand(NUM_PARTICLES, 2)

                    # TODO: kako bi prisil particle da so na zacetku okoli object bounda?
                    s_t[:, 0] = single_det[1]
                    s_t[:, 1] = single_det[0]

                    w_t = np.ones(NUM_PARTICLES) / NUM_PARTICLES
                    likelihood = np.zeros(NUM_PARTICLES)
                    tracked = ParticleFilter(s_t, interesting, w_t, likelihood, object_bound, i,0)
                    temp_trackers.append(tracked)
            trackers = temp_trackers

            key = cv2.waitKey(1) & 0xFF

done = time.time()
elapsed = done - start
print(elapsed)